//IMPORTS
const express = require("express");
const app = express();
const dotenv = require("dotenv");
const logger = require("morgan");
const router = express.Router();

//JWT SECRET
app.set("secretKey", "SportApi"); 

//ROUTES IMPORT
const rental = require('./routes/rental.routes')
const users = require('./routes/users.routes')

//OTHERS
const HTTPSTATUSCODE = require("./utils/httpStatusCode");


//DB CONNECTION
const { connect } = require("./utils/db");
connect();

//ENV
const PORT = process.env.API_PORT;

// SET HEADERS

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
  });

// CORS
const cors = require("cors");

app.use(
  cors({
    origin: ["http://localhost:1900", "http://localhost:4200"],
    credentials: true,
  })
);

//JSON APPLIED
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//MORGAN 
app.use (logger("dev"));




//ROUTES
app.use("/rental", rental);
app.use("/users", users);




//ERROR

app.use((req, res, next) => {
    let err = new Error();
    err.status = 404;
    err.message = HTTPSTATUSCODE[404];
    next(err);
  });

  // handle errors
app.use((err, req, res, next) => {
    return res.status(err.status || 500).json(err.message || 'Unexpected error');
  })


  //SERVER LISTEN 

  app.disable('x-powered-by');

  app.listen(PORT, () => {
    console.log(`server running in http://localhost:${PORT}`);
  });