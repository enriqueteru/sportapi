const express = require("express");
const router = express.Router();


// IMPORT USERS CONTROLLERS 
const {
  createUser,
  AuthUser,
  editUser,
  logout,
} = require("../controllers/user.controller");

//MIDDLEWARE SECURE 
const { isAuth } = require("../middleware/auth.middleware")


router.post("/register", createUser);
router.post("/authenticate", AuthUser);
router.put("/edit/:id", editUser)

router.post("/logout", [isAuth], logout)








module.exports = router;
