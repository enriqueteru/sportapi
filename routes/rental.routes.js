const express = require("express");
const router = express.Router();


const { getAllRentals, getRentalById, getAllRentalsByUser, newRental, deleteRentalById, } = require("../controllers/rental.controller");
const {isAuth} = require('../middleware/auth.middleware')
//GET ROUTES
router.get("/",[isAuth], getAllRentals);
router.get("/:rentalId",[isAuth], getRentalById);
router.get("/:rentalByUser", [isAuth], getAllRentalsByUser)
router.post("/new-rental",[isAuth], newRental),
router.delete("/delete/deleteRentalId", [isAuth],deleteRentalById),

module.exports = router;