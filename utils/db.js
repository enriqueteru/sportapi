const dotenv = require("dotenv");
dotenv.config();
const mongoose = require("mongoose");
const MONGO_URI = process.env.MONGO_URI;
const connect = async () => {
  try {
    const db = await mongoose.connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    const { name, host } = db.connection;
    console.log(`connect with db: ${name}, in host ${host}`);
  } catch (error) {
    console.log("error ----> ", error);
  }
};

module.exports = { connect };
