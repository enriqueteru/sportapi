const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RentalSchema = new Schema(
  {
    name: { type: String, require: true },
    categorie: { type: String, require: true },
    year: { type: String, require: true },
    model: { type: String, require: true },
    pricePerDay: { type: String, require: true },
    insurance: { type: Boolean, require: true},
    description: { type: String, require: true },
    details: { type: String, require: false },
    author: { type: Schema.Types.ObjectId, ref: "Users_sport", required: true},
  },

  { timestamps: true }
);

const Rental = mongoose.model("Rentals_sport", RentalSchema);
module.exports = Rental;
