const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//BCRYPT 
const bcrypt = require('bcryptjs');
const saltRounds = 10;

const UserSchema = new Schema(
  {
    name: { type: String, require: true },
    subname_1: { type: String, require: true },
    subname_2: { type: String, require: true },
    DNI: { type: String, require: true },
    password: { type: String, require: true },
    //passwordValidation: { type: String, require: true },
    phone: { type: Number, require: true},
    email: { type: String, require: true },
    role: {type: String, require: true},
    googleAuth: { type: Boolean},

    address: {
      country: { type: String, require: true },
      city: { type: String, require: true },
      postalCode: { type: String, require: true },
      street: {
        type: String,
        require: true,
      },
      number: {
        type: Number,
        require: true,
      },
      additionalData: {
        type: String,
        require: false,
      },
    },
  },

  { timestamps: true }
);

// BCRYPT MAGIC
UserSchema.pre("save", function (next) {
  this.password = bcrypt.hashSync(this.password, saltRounds);
  //this.passwordValidation = bcrypt.hashSync(this.passwordValidation, saltRounds);
  next();
});

const User = mongoose.model("Users_sport", UserSchema);
module.exports = User;