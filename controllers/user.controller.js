const User = require("../models/User.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const HTTPSTATUSCODE = require("../utils/httpStatusCode");


//CREATE USER

const createUser = async (req, res, next) => {
const {email} = req.body;


try {
const newUser = new User(req.body);
const userExist =  await User.findOne({email});

if(userExist) {
  return res.status(400).json({
    ok: false,
    msg: 'Email is in the DATABASE'
  })
}

  const userDb = await newUser.save();
  
  return res.json({
    status: 201,
    message: HTTPSTATUSCODE[201],
    data: userDb,
  });


  } catch (error) {
    return next(error);
  }};



//EDIT

const editUser = async (req,res, next) => {
  const uid = req.params.id
  try{


    const UserIdFinder = await User.findById(uid)

    if(!UserIdFinder){
      return res.status(404).json({
        ok: false,
        msg: "User ID IS NOT IN DATABASE"
      });
    }

  const dataReq = req.body;

if(UserIdFinder.email === req.body.email){
delete dataReq.email;
} else{ 
  const emailFinder = await User.findOne({email: req.body.email})
  if(emailFinder){
    return res.status(404).json({
      msg: "Email in USE"
    })
  }
}
      
      delete dataReq.password;
      const updateUser = await User.findByIdAndUpdate(uid, dataReq, { new: true});
      
      return res.json({
        updateUser,
        msg: "USER UPDATE"
      });
    



  }catch(err){
    return next(err);
  
  }}

//AUTH USERS
const AuthUser = async (req, res, next) => {
  try {
    const findUser = await User.findOne({name: req.body.name});

    if (bcrypt.compareSync(req.body.password, findUser.password)) {
      findUser.password = null;

      const token = jwt.sign(
        {
          id: findUser._id,
          name: findUser.name,
        },
        req.app.get("secretKey"),
        { expiresIn: "24h" }
      );

      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        data: { user: findUser, token: token },
      });
    } else {
      return res.json({
        status: 400,
        message: HTTPSTATUSCODE[400],
        data: null,
      });
    }
  } catch (error) {
    return next(error);
  }
};

//LOGOUT
const logout = (req, res, next) => {
    try {
      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        token: null
      });
    } catch (err) {
      return next(err)
    }
  }
  
  module.exports = {
    createUser,
    AuthUser,
    editUser,
    logout
  }