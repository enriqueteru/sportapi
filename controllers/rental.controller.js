const Rental = require("../models/Rental.model");
const User = require("../models/User.model");
const HTTPSTATUSCODE = require('../utils/httpStatusCode')

//GET ALL RENTALS

const getAllRentals = async (req, res, next) => {
  try {
    if (req.query.page) {
      //Se le añade paginación
      const page = parseInt(req.query.page);
      const skip = (page - 1) * 20;
      const rentals = await Rental.find().skip(skip).limit(20);
      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        data: { rentals: rentals },
      });
    } else {
      const rentals = await Rental.find();
      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        data: { rentals: rentals },
      });
    }
  } catch (err) {
    return next(err);
  }
};




// GET RENTAL BY ID
const getRentalById = async (req, res, next) => {
  try {
    const { rentalId } = req.params;
    const rentalById = await Rental.findById(rentalId);
    return res.json({
      status: 200,
      message: HTTPSTATUSCODE[200],
      data: { rental: rentalById },
    });
  } catch (err) {
    return next(err);
  }
};

//GET ALL RENTAL BY USER ID

const getAllRentalsByUser = async (req, res, next) => {
  try {
    const author = req.authority.id;

    if (req.query.page) {
      const page = parseInt(req.query.page);
      const skip = (page - 1) * 20;
      const allRentalsByUser = await Rental.find({ author: author }).skip(skip).limit(20).populate("Users_sport");
      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        data: { rentals: allRentalsByUser },
      });
    } else {Rental.find({ author: author }).populate("rentals");
      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        data: { rentals: allRentalsByUser },
      });
    }
  } catch (error) {
    return next(error)
  }
}



// CREATE NEW RENTAL
const newRental = async (req, res, next) => {
  try {
    const newRental = new Rental(req.body);
    //newRental.author = req.authority.id;
    const rentalDb = await newRental.save();
    return res.json({
      status: 201,
      message: HTTPSTATUSCODE[201],
      data: { Rental: rentalDb },
    });
  } catch (error) {
    return next(error);
  }
};


//DELETE RENTAL
const deleteRentalById = async (req, res, next) => {
  try {
    const { rentalId } = req.params;
    const authority = req.authority.id
    const userRetal = await Rental.findById(rentalId)

    if (authority == userRental.author._id) {

      const rentalDeleted = await rental.findByIdAndDelete(rentalId);
      if (!rentalDeleted) {
        return res.json({
          status: 200,
          message: "There is not a rental with that Id",
          data: null
        })
      } else {
        return res.json({
          status: 200,
          message: HTTPSTATUSCODE[200],
          data: { rental: rentalDeleted },
        });
      }
    } else {
      return res.json({
        status: 403,
        message: HTTPSTATUSCODE[403],
        data: null
      })
    }
  } catch (error) {
    return next(error);
  }
};

//UPDATE RENTAL

const updateRentalById = async (req, res, next) => {
  try {
    const { rentalId } = req.params;
    const authority = req.authority.id
    const userRental = await User.findById(UserId)

    if (authority == userRental.author._id) {

      const rentalToUpdate = new Rental();
      if (req.body.name) rentalToUpdate.name = req.body.name;
      if (req.body.description) rentalToUpdate.description = req.body.description;

      rentalToUpdate._id = rentalId;

      const rentalUpdated = await Rental.findByIdAndUpdate(rentalId, rentalToUpdate);
      return res.json({
        status: 200,
        message: HTTPSTATUSCODE[200],
        data: { rental: rentalUpdated }
      });
    } else {
      return res.json({
        status: 403,
        message: HTTPSTATUSCODE[403],
        data: null
      })
    }

  } catch (err) {
    return next(err);
  }
}





module.exports = {
  getAllRentals,
  getRentalById,
  newRental,
  updateRentalById,
  getAllRentalsByUser,
  deleteRentalById 

};
